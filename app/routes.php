<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

Route::group(array('prefix' => 'api/v1', 'before' => 'apiauth'), function()
{

	Route::resource('/', 'HomeController');
   	
   Route::post('novovendedor', 'SellersController@cadastrar');   
   Route::post('contratar', 'ProductsController@contratar');

   	// outros
   Route::post('advertising/create', 'AdvertisingController@createAdvertising');
	Route::resource('advertising', 'AdvertisingController');
   Route::post('productuser', 'ProductsUserController@getProducts');
	Route::post('productedit', 'ProductsUserController@getProductsEdit');
   Route::post('productupdate', 'ProductsUserController@productsUpdate');

   
   Route::post('productdesativar', 'ProductsController@desativar');

   Route::post('pesquisar', 'ProductsController@pesquisar');

      // posts
   Route::resource('posts', 'PostsController');
   Route::post('posts/contposts', 'PostsController@contPosts');
   Route::resource('post/ativar', 'PostsController@ativar');
   Route::resource('post/remover', 'PostsController@remover');
   Route::post('post/responder', 'PostsController@responder');

   Route::post('postsbuyer', 'PostsController@postsbuyer');


   Route::post('productregion', 'ProductsController@productRegion');
      // regiões
   Route::resource('brasil', 'ProductsController');
   Route::resource('amazonas', 'ProductsController');
   Route::resource('acre', 'ProductsController');
   Route::resource('alagoas', 'ProductsController');
   Route::resource('amapa', 'ProductsController');
   Route::resource('ceara', 'ProductsController');
   Route::resource('distritofederal', 'ProductsController');
   Route::resource('espiritosanto', 'ProductsController');
   Route::resource('maranhao', 'ProductsController');
   Route::resource('parana', 'ProductsController');
   Route::resource('pernambuco', 'ProductsController');
   Route::resource('parana', 'ProductsController');
   Route::resource('pernambuco', 'ProductsController');
   Route::resource('piaui', 'ProductsController');
   Route::resource('riograndedonorte', 'ProductsController');
   Route::resource('riograndedonsul', 'ProductsController');
   Route::resource('rondonia', 'ProductsController');
   Route::resource('roraima', 'ProductsController');
   Route::resource('santacatarina', 'ProductsController');
   Route::resource('sergipe', 'ProductsController');
   Route::resource('tocantins', 'ProductsController');
   Route::resource('para', 'ProductsController');
   Route::resource('bahia', 'ProductsController');
   Route::resource('goias', 'ProductsController');
   Route::resource('matogrosso', 'ProductsController');
   Route::resource('matogrossodosul', 'ProductsController');
   Route::resource('riodejaneiro', 'ProductsController');
   Route::resource('saopaulo', 'ProductsController');
   Route::resource('riograndedosul', 'ProductsController');
   Route::resource('minasgerais', 'ProductsController');
   Route::resource('paraiba', 'ProductsController');
  
      // private
   
   Route::post('private/todasmensagens', 'PrivateController@todasMensagens');
   Route::post('private/status', 'PrivateController@getUserStatus');
   Route::post('private/getpost', 'PrivateController@getPosts');
   Route::post('private/getposts', 'PrivateController@getPostsShow');
});

Route::resource('minasgerais', 'ProductsController');

/*
   Route::resource('private/todasmensagens', 'PrivateController@todasMensagens');
   Route::resource('pesquisar', 'ProductsController@pesquisar');

   Route::resource('posts/contposts', 'PostsController@contPosts');
   Route::resource('post/responder', 'PostsController@responder');
   Route::resource('private/getposts', 'PrivateController@getPostsShow');
   Route::resource('private/getpost', 'PrivateController@getPosts');
   Route::resource('private/status', 'PrivateController@getUserStatus');
   Route::resource('post/remover', 'PostsController@remover');
   Route::resource('post/ativar', 'PostsController@ativar');
   Route::resource('postsbuyer', 'PostsController@postsbuyer');

	// outros
   Route::resource('advertising/create', 'AdvertisingController@createAdvertising');
	Route::resource('advertising', 'AdvertisingController');
   Route::resource('productuser', 'ProductsUserController');
   Route::resource('productedit', 'ProductsUserController@getProductsEdit');
   Route::resource('productupdate', 'ProductsUserController@productsUpdate');
   

   Route::resource('productregion', 'ProductsController@productRegion');

	//brasil
 	Route::resource('brasil', 'ProductsController');

 	   	//estados
	Route::resource('parana', 'ProductsController');
   Route::resource('riograndedonorte', 'ProductsController');

Route::resource('posts', 'PostsController');

Route::resource('post_products', 'Post_productsController');
*/