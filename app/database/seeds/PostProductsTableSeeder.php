<?php

class PostProductsTableSeeder extends Seeder {

    public function run()
    {
    	// Uncomment the below to wipe the table clean before populating
    	// DB::table('post_products')->delete();

        $post_products = array(
        	array(
        		'post_id'		=> 1,
        		'product_id'	=> 1				
			),
			array(
				'post_id'		=> 2,
        		'product_id'	=> 1				
			)
        );

        // Uncomment the below to run the seeder
        // DB::table('post_products')->insert($post_products);
    }

}