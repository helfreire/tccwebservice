<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('UserTableSeeder');
		$this->call('SellersTableSeeder');
		$this->call('ContactsTableSeeder');
		$this->call('ProductsTableSeeder');
		$this->call('AddressesTableSeeder');
		$this->call('AddressesTableSeeder');
		$this->call('CategoriesTableSeeder');
		$this->call('SubcategoriesTableSeeder');
		$this->call('CategoriesProductsTableSeeder');
		$this->call('ImagesTableSeeder');
		$this->call('PostsTableSeeder');
		$this->call('PostProductsTableSeeder');
	}

}