<?php

class ContactsTableSeeder extends Seeder {

    public function run()
    {
    	// Uncomment the below to wipe the table clean before populating
    	 DB::table('contacts')->delete();

        $contacts = array(
        	array(
		        	'address' => 'Endereço teste 1',
		            'phone' => 4123344334,
		            'mobile' => 11989888989,
		            'seller_id' => 1
	             ),
            array(
		        	'address' => 'Endereço teste 2',
		            'phone' => 4123344334,
		            'mobile' => 11989888989,
		            'seller_id' => 2
	            )
        );

        // Uncomment the below to run the seeder
         DB::table('contacts')->insert($contacts);
    }

}