<?php

class CategoriesProductsTableSeeder extends Seeder {

    public function run()
    {
    	// Uncomment the below to wipe the table clean before populating
    	DB::table('category_product')->delete();

        $category_product = array(
        	array(
	        	'product_id' => 1,
	        	'category_id'	=> 1,
	        	'subcategory_id' => 1
        	),
        	array(
	        	'product_id' => 2,
	        	'category_id'	=> 2,
	        	'subcategory_id' => 2
        	)
        );

        // Uncomment the below to run the seeder
        DB::table('category_product')->insert($category_product);
    }

}