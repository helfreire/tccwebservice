<?php

class PostsTableSeeder extends Seeder {

    public function run()
    {
    	// Uncomment the below to wipe the table clean before populating
    	// DB::table('posts')->delete();

        $posts = array(
        	array(
	        	'product_id'	=> 1,
                'seller_id'    => 1,
                'comment' => 'comentario padrao',
	        	'name'	=> 'Heleno Freire',
	        	'email' => 'helfreire@msn.com',
	        	'action'		=> 'question',
	        	'created_at'	=> '2013-10-30 00:10:00'
        	),
        	array(
	        	'product_id'	=> 1,
                'seller_id'    => 1,
                'comment' => 'comentario padrao',
	        	'name'	=> 'Eduardo ',
	        	'email' => 'edu@hotmail.com',
	        	'action'		=> 'answer',
	        	'created_at'	=> '2013-10-30 00:10:00'
        	)
        );

        // Uncomment the below to run the seeder
        // DB::table('posts')->insert($posts);
    }

}