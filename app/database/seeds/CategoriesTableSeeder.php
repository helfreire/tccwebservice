<?php

class CategoriesTableSeeder extends Seeder {

    public function run()
    {
    	// Uncomment the below to wipe the table clean before populating
    	DB::table('categories')->delete();

        $categories = [
            [
                'category_name' => 'Animais'
            ],
            [
                'category_name' => 'Bêbes e crianças'
            ],
            [
                'category_name' => 'Eletrônicos - Telefonia'
            ],
            [
                'category_name' => 'Empregos e negócios'
            ],
            [
                'category_name' => 'Esportes'
            ],
            [
                'category_name' => 'Hobbies - Música'
            ],
        	[
        		'category_name'	=> 'Imóveis'
        	],
            [
                'category_name' => 'Veículos e barcos'
            ],
            [
                'category_name' => 'Moda e Beleza'
            ],
            [
                'category_name' => 'Para sua casa'
            ]
        ];

        // Uncomment the below to run the seeder
        DB::table('categories')->insert($categories);
    }

}