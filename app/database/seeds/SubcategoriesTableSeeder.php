<?php

class SubcategoriesTableSeeder extends Seeder {

    public function run()
    {
    	// Uncomment the below to wipe the table clean before populating
    	DB::table('subcategories')->delete();

        $subcategories = [
        	[
                'category_id'      => 1,
	        	'subcategory_name' => 'Cachorros'
	        ],
            [
                'category_id'      => 1,
                'subcategory_name' => 'Acessórios'
            ],
            [
                'category_id'      => 1,
                'subcategory_name' => 'Outros animais'
            ],
            [
                'category_id'      => 2,
                'subcategory_name' => 'Bêbes e crianças'
            ],
            [
                'category_id'      => 3,
                'subcategory_name' => 'Celulares e Acessórios'
            ],
            [
                'category_id'      => 3,
                'subcategory_name' => 'Computadores e Periféricos'
            ],
            [
                'category_id'      => 3,
                'subcategory_name' => 'Video Games - Consoles'
            ],
            [
                'category_id'      => 3,
                'subcategory_name' => 'Câmeras e Acessórios'
            ],
            [
                'category_id'      => 3,
                'subcategory_name' => 'TV - Áudio -Video e fotografia'
            ],
            [
                'category_id'      => 4,
                'subcategory_name' => 'Serviços'
            ],
            [
                'category_id'      => 4,
                'subcategory_name' => 'Ofertas de emprego'
            ],
            [
                'category_id'      => 4,
                'subcategory_name' => 'Procuro emprego'
            ],
            [
                'category_id'      => 4,
                'subcategory_name' => 'Negócio - Indústria - Agro'
            ],
            [
                'category_id'      => 5,
                'subcategory_name' => 'Artigos esportivos e ginastica'
            ],
            [
                'category_id'      => 5,
                'subcategory_name' => 'Ciclismo'
            ],
            [
                'category_id'      => 6,
                'subcategory_name' => 'Livros - Revistas'
            ],
            [
                'category_id'      => 6,
                'subcategory_name' => 'Instrumentos Musicais'
            ],
            [
                'category_id'      => 6,
                'subcategory_name' => 'Jogos - Brinquedos'
            ],
            [
                'category_id'      => 6,
                'subcategory_name' => 'Antiguidades'
            ],
            [
                'category_id'      => 6,
                'subcategory_name' => 'Músicas e filmes (DVDs, etc.)'
            ],
            [
                'category_id'      => 7,
                'subcategory_name' => 'Vendas - Casas e Apartamentos'
            ],
            [
                'category_id'      => 7,
                'subcategory_name' => 'Aluguel - Casas e Apartamentos'
            ],
            [
                'category_id'      => 7,
                'subcategory_name' => 'Terrenos - Sítios e Fazendas'
            ],
            [
                'category_id'      => 7,
                'subcategory_name' => 'Temporada'
            ],
            [
                'category_id'      => 7,
                'subcategory_name' => 'Lojas - Pontos Comerciais'
            ],
            [
                'category_id'      => 7,
                'subcategory_name' => 'Imóveis Comerciais - Escritórios e outros'
            ],
            [
                'category_id'      => 7,
                'subcategory_name' => 'Aluguel de Quartos'
            ],
            [
                'category_id'      => 8,
                'subcategory_name' => 'Carros'
            ],
            [
                'category_id'      => 8,
                'subcategory_name' => 'Motos'
            ],
            [
                'category_id'      => 8,
                'subcategory_name' => 'Caminhões - Ônibus - Vans'
            ],
            [
                'category_id'      => 8,
                'subcategory_name' => 'Peças e Acessórios'
            ],
            [
                'category_id'      => 8,
                'subcategory_name' => 'Barcos - Lanchas - Aviões'
            ],
            [
                'category_id'      => 9,
                'subcategory_name' => 'Roupas e calçados'
            ],
            [
                'category_id'      => 9,
                'subcategory_name' => 'Bijouterias, relógios e joias'
            ],
            [
                'category_id'      => 9,
                'subcategory_name' => 'Beleza e Saúde'
            ],
            [
                'category_id'      => 9,
                'subcategory_name' => 'Malas, mochilas e acessórios'
            ],
            [
                'category_id'      => 10,
                'subcategory_name' => 'Móveis e Decoração'
            ],
            [
                'category_id'      => 10,
                'subcategory_name' => 'Eletrodomésticos'
            ],
            [
                'category_id'      => 10,
                'subcategory_name' => 'Jardinagem e Construção'
            ],
            [
                'category_id'      => 10,
                'subcategory_name' => 'Para Bebês e Crianças'
            ],
            [
                'category_id'      => 10,
                'subcategory_name' => 'Cama - Mesa e Banho'
            ],
            [
                'category_id'      => 10,
                'subcategory_name' => 'Eletrodomésticos'
            ]
        ];

        // Uncomment the below to run the seeder
        DB::table('subcategories')->insert($subcategories);
    }

}