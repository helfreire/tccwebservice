<?php

class ProductsTableSeeder extends Seeder {

    public function run()
    {
    	// Uncomment the below to wipe the table clean before populating
    	DB::table('products')->delete();

        $products = array(
        	array(
	        	'title'	=> 'Produto anuncio 1',
                'description' => 'Descrição produto 1',
	        	'price'	=> 100.20,
	        	'country' => 'PR',
	        	'city'		=> 'Curitiba',
	        	'status'	=> '1',
	        	'seller_id'	=> 1
        	),
        	array(
	        	'title'	=> 'Produto anuncio 2',
                'description' => 'Descrição produto 2',
	        	'price'	=> 200.20,
	        	'country' => 'SP',
	        	'city'		=> 'SAO PAULO',
	        	'status'	=> '1',
	        	'seller_id'	=> 2
        	)
        );
        // Uncomment the below to run the seeder
        DB::table('products')->insert($products);
    }

}