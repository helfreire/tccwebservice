<?php

class AddressesTableSeeder extends Seeder {

    public function run()
    {
    	// Uncomment the below to wipe the table clean before populating
    	DB::table('addresses')->delete();

        $addresses = array(
        	array(
	        	'address' => "rua produto 1",
	        	'phone'	=> 4189899090,
	        	'mobile'	=> 4198988776,
	        	'product_id'	=> 1
        	),
        	array(
	        	'ddress' => "rua produto 2",
	        	'phone'	=> 418989945,
	        	'mobile'	=> 4198988980,
	        	'product_id'	=> 2
        	)
        );

        // Uncomment the below to run the seeder
        DB::table('addresses')->insert($addresses);
    }

}