<?php

class SellersTableSeeder extends Seeder {

    public function run()
    {

		DB::table('sellers')->delete();

        Seller::create(array(
            'user' => 'tcc1',
            'firstname'	=> 'Teste',
            'lastname'	=> 'um',
            'password' => Hash::make('tcc')
        ));

        Seller::create(array(
            'user' => 'tcc2',
            'firstname'	=> 'Teste',
            'lastname'	=> 'dois',
            'password' => Hash::make('tcc')
        ));
    }

}