<?php

class ImagesTableSeeder extends Seeder {

    public function run()
    {
    	// Uncomment the below to wipe the table clean before populating
    	DB::table('images')->delete();

        $images = array(
        	array(
        		'product_id' => 1,
	        	'image'	=> URL::previous() . "/images/product-1/",
                'quantity'  => 4
        	),
        	array(
        		'product_id' => 2,
	        	'image'	    => URL::previous() . "/images/product-2/",
	        	'quantity'	=> 4,
        	)
        );

        // Uncomment the below to run the seeder
        DB::table('images')->insert($images);
    }

}