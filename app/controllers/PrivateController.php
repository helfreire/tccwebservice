<?php

class PrivateController extends BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return Response::json('private');
    }

    public function getUserStatus(){
        return Response::json( Product::with(
                    'address', 'categories', 'subcategories',
                    'images', 'posts' )
                    ->where('seller_id', '=', Input::get('seller_id'))
                    ->get());
    }

    public function getPosts(){
        return Response::json( Product::with('postsDesc', 'images')
                ->where('seller_id', '=', Input::get('seller_id') )
                ->orderBy('created_at', 'DESC')
                ->get());
    }

    public function getPostsShow(){
        //Input::get('post_id')
        $post       = Post::find( Input::get('post_id') );
        
        $post->view = 1;
        $post->save();
        $product    = Product::with('images', 'posts')->find($post->product_id);

        return Response::json( $product );
    }

    public function todasMensagens(){
        return Response::json( Product::with('images', 'posts')
                            ->find( Input::get('product_id') ) );
    }
}