<?php

class PostsController extends BaseController {

    public function postsbuyer(){
        $post               = new Post();
        $post->comment      = Input::get('comment');
        $post->name         = Input::get('name');
        $post->email        = Input::get('email');
        $post->product_id   = Input::get('product_id');
        $post->status       = 1;
        $post->action       = 'question';
        
        $post->save();


        $post_product               = new Post_product();
        $post_product->post_id      = $post->id;
        $post_product->product_id   = Input::get('product_id');
        $post_product->save();
        
        return Response::json( [ 'error' => false, 'post_id' => $post->id  ] );
    }

    public function ativar($id){
        /*
        $userpost   = DB::table('products')
                        ->join('posts', 'products.id', '=', 'posts.product_id')
                        ->where('products.seller_id', '=', Input::get('seller_id') )
                        ->where('posts.id', '=', Input::get('id') )
                        ->get();

        if( count($userpost) ){
            */
            $post           = Post::find( $id );
            $post->status   = 1;
            $post->save();
            return Response::json( $post );            
        //}

       //return Response::json( 'post invalido para este vendedor' );
        
    }

    public function remover($id){
        $post_product           = Post_product::where('post_id', '=', $id);
        $post_product->delete();

        $post       = Post::find($id);
        $post->delete();

        return Response::json( $post );            
    }


    public function responder (){

        $seller = Seller::with('product')->find( Input::get('seller_id') );
        $nome = $seller->firstname . ' ' . $seller->lastname;

        $post   = new Post();
        $post->product_id   = Input::get('product_id');
        $post->comment      = Input::get('comment');
        $post->name         = $nome;
        $post->action       = 'answer';
        $post->status       = 1;
        $post->view         = 1;
        $post->save();

        $post_product               = new Post_product();
        $post_product->post_id      = $post->id;
        $post_product->product_id   = Input::get('product_id');
        $post_product->save();

        return Response::json( $post );

    }


    public function contPosts(){
        
        return Response::json( DB::table('products')
                    ->select(DB::raw('count(posts.id) as posts'))
                    ->join('posts', 'products.id', '=', 'posts.product_id')
                    ->where('posts.view', '=', 0)
                    ->where("posts.action", '=', 'question')
                    ->where('products.seller_id', '=', Input::get('seller_id') )
                    ->first());
                    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return Response::json( Post::where("product_id", "=", $id)
            ->where('status', '=', '1')
            ->get());
    }
}