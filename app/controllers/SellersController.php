<?php

class SellersController extends BaseController {

   public function cadastrar(){

        $seller     = new  Seller();
        $seller->id         = Input::get('seller_id');
        $seller->firstname  = Input::get('first_name');
        $seller->lastname   = Input::get('last_name');
        $seller->user       = Input::get('user');
        $seller->save();
        
        $contact                = new Contact();
        $contact->address       = Input::get( 'address' );
        $contact->email         = Input::get( 'email' );
        $contact->phone         = Input::get( 'phone' );
        $contact->mobile        = Input::get( 'mobile' );
        $contact->seller_id     = $seller->id;

        $contact->save();

        return Response::json( array('error' => false) );         
   }

}