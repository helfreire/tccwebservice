<?php

class ProductsController extends BaseController {

    public $restful = true;

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        if( array_key_exists(self::formatRouter(), $this->estados )){
            return Response::json(Product::with(
                    'address', 'categories', 'subcategories', 'images')
                    ->where('country', '=', $this->estados[self::formatRouter()])
                    ->where('status', '=', 1)
                    ->orderBy('priority', 'desc')
                    ->get());
        }

        return Response::json(Product::with(
                    'address', 'categories', 'subcategories', 'images')
                    ->orderBy('priority', 'desc')
                    ->where('status', '=', 1)
                    ->get());
    }

    public function productRegion(){
        if( array_key_exists( Input::get( 'city' ), $this->estados )){
            return Response::json( Product::whereRaw( "products.id in ( 
                                        SELECT product_id 
                                            FROM category_product
                                            WHERE category_id = " . $this->categorias[ Input::get('category')] ." ) ")
                    ->with( 'address', 'categories', 'subcategories', 'images')
                    ->where('country', '=', $this->estados[ 'parana' ])
                    ->orderBy('priority', 'desc')
                    ->get() );
        }

        return Response::json( Product::whereRaw( "products.id in ( 
                                        SELECT product_id 
                                            FROM category_product
                                            WHERE category_id = " . $this->categorias[ Input::get('category')] ." ) ")
                    ->with( 'address', 'categories', 'subcategories', 'images')
                    ->orderBy('priority', 'desc')
                    ->get() );       
                    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id){
        $produp = Product::find($id) ;
        $produp->increment('view');
        $produtos = Product::with(
                'address', 'categories', 'subcategories', 'images')
                ->find($id)->toArray();

        $vendedor = Seller::find( $produtos['seller_id'] )->toArray();
        return Response::json( array_merge($produtos, [ 'seller' => $vendedor ] ) );
    }

    public function pesquisar(){
        return Response::json( Product::with(
                    'address', 'categories', 'subcategories', 'images')
                    ->where('title', 'like', '%' . Input::get('pesquisa') . '%')
                    ->orderBy('priority', 'desc')
                    ->get() ); 
    }

    public function contratar(){

        $products = Product::find( Input::get('product_id') );
        $products->lastdate =  date('Y-m-d', strtotime("+" . Input::get('periodo') . "days"));
        $products->priority = 1;
        $products->save();
        return Response::json( $products );
    }

    public function desativar(){
        $products = Product::find( Input::get('product_id') );
        $products->status = 0;
        $products->save();
        return Response::json( $products );
    }
}