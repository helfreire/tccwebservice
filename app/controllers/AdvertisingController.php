<?php

class AdvertisingController extends BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //return Response::json( Category::with('subcategories')->get() ); 
        
        $categories =  Category::all();
        $result = array();
        foreach ($categories as $category) {
            $subcategories    = Subcategory::where('category_id', '=', $category->id )->get();
            foreach ($subcategories as $subcategory) {
                $result[$category->id][$category->category_name][$subcategory->id] = $subcategory->subcategory_name;
            }
        }

        return Response::json( $result ); 

        Log::info( $result );
        return View::make('debug');
        
        
    }

    public function createAdvertising(){

        //return Response::json( $files = Input::all()); 
         
        $product                = new Product();
        $product->title         = Input::get('title');
        $product->description   = Input::get('description');
        $product->price         = Input::get('price');
        $product->country       = Input::get('country');
        $product->city          = Input::get('city');
        $product->seller_id     = Input::get('seller_id');
        $product->status        = 1;
        $product->save();

        $addresses              = new Address();
        $addresses->address     = Input::get('address');
        $addresses->phone       = Input::get('phone');
        $addresses->mobile      = Input::get('mobile');
        $addresses->product_id  = $product->id;
        $addresses->save();

        $subcategory                        = Subcategory::find( Input::get('categorias') );

        $category_product                   = new Category_Product();
        $category_product->product_id       = $product->id;
        $category_product->category_id      = $subcategory->category_id;
        $category_product->subcategory_id   = Input::get('categorias');
        $category_product->save();
    

        //$path = "/var/www/images/product-" . $product->id . '/';
        $path = "http://localhost/images/product-{$product->id}/";

        $images                 = new Image();
        $images->product_id     = $product->id;
        $images->image          = $path;
        $images->quantity       = Input::get('quantity');
        
        $images->save();

        return Response::json( [ 'error'        => false, 
                                'product_id'    => $product->id,
                                'country'       => Input::get('country') ] );
    }
}