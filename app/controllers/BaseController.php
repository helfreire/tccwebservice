<?php

class BaseController extends Controller {

	public $estados = array( 
            'amazonas'            => 'AM',
            'acre'  => 'AC',
            'alagoas'  => 'AC',
            'amapa'  => 'AP',
            'ceara'  => 'CE',
            'distritofederal'  => 'DF',
            'espiritosanto'  => 'ES',
            'parana'  => 'PR',
            'pernambuco'  => 'PE',
            'piaui'  => 'PI',
            'riograndedonorte'  => 'RN',
            'riograndedonsul'  => 'RS',
            'rondonia'  => 'RO',
            'roraima'  => 'RR',
            'santacatarina'  => 'SC',
            'sergipe'  => 'SE',
            'tocantins'  => 'TO',
            'para'  => 'PA',
            'bahia'  => 'BH',
            'goias'  => 'GO',
            'matogrosso'  => 'MT',
            'saopaulo'  => 'SP',
            'riograndedosul'  => 'RS',
            'minasgerais'  => 'MG',
            'paraiba'  => 'PB',
         );

	public $categorias = array(
			'animais'				=> 1,
			'bebesecriancas'		=> 2,
			'eletronicostelefonia'	=> 3,
			'empregosenegocios'		=> 4,
			'esportes'				=> 5,
			'hobbiesmusica'			=> 6,
			'imoveis'				=> 7,
			'veiculosebarcos'		=> 8,
			'modaebeleza'			=> 9,
			'parasuacasa'			=> 10
		);

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

	public function formatRouter(){
        $route = explode('.', Route::currentRouteName());
        if(count($route) == 2)
        	return $route[0];

        return $route[2];
    }

}