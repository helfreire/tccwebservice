<?php

class ProductsUserController extends BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return Response::json( 'index' );
    }

    public function getProducts(){
        return Response::json(Product::with(
                    'address', 'categories', 'subcategories', 'images')
                    ->where('seller_id', '=', Input::get('seller_id') )
                    ->get());
    }

    public function getProductsEdit()
    {
        return Response::json(Product::with(
                    'address', 'categories', 'subcategories', 'images')
                    ->where('seller_id', '=', Input::get('seller_id') )
                    ->where('id', '=', Input::get('product_id') )
                    ->get());
    }

    public function productsUpdate(){
        
           
        // atualiza dados produtos     
        $product                = Product::find( Input::get('produt_id') );
        $product->title         = Input::get('title');
        $product->description   = Input::get('description');
        $product->price         = Input::get('price');
        $product->country       = Input::get('country');
        $product->city          = Input::get('city');
        $product->save();

        // atualiza dados contato
        $addresses              = Address::where('product_id', '=', 1)
                                    ->first();
        $addresses->address     = Input::get('address');
        $addresses->phone       = Input::get('phone');
        $addresses->mobile      = Input::get('mobile');
        $addresses->save();
    
        // atualiza dados categoria

        $categories         = Subcategory::find( Input::get('categorias'));
        $category_product   = Category_Product::where('product_id', '=', Input::get('produt_id') )
                                    ->first();
        
        $category_product->category_id      = $categories->category_id;
        $category_product->subcategory_id   = $categories->id;
        $category_product->save();
        
        return Response::json( array('error' => false) );
    }

}