<?php

class NovoController extends BaseController {

	public function __construct()
	{
	    $this->beforeFilter('apiauth');
	}
	
	public function index()
	{
	    return 'novo';
	}
}