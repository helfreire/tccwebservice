<?php

class Category extends Eloquent {

	protected $hidden = array( 'created_at', 'updated_at');
	
	public function categoryProducts(){
    
        return $this->belongsToMany('Product');
    }

    public function subcategories(){
    	return $this->belongsToMany('Subcategory');	
    }
}