<?php

class Post_product extends Eloquent {
    protected $guarded = array();
    protected $table = 'post_product';
    public static $rules = array();

    public $timestamps = false;

    public function posts(){
    	return $this->hasOne('Post', 'id');
    }
}