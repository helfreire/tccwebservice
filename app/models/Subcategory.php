<?php

class Subcategory extends Eloquent {
    
	protected $hidden = array( 'created_at', 'updated_at');
    
    public function subCategoryProducts(){
    
        return $this->belongsToMany('Product');
    }
}