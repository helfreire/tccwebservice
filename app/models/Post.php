<?php

class Post extends Eloquent {
    protected $guarded = array();

    public static $rules = array();

    public function products(){
		return $this->hasOne('Product', 'id' );
	}

	public function seller(){
		return $this->hasOne('Seller');
	}
}