<?php

class Contact extends Eloquent {
   
	protected $hidden = array( 'created_at', 'updated_at');

   public function seller(){
   		return 	$this->belongsTo('Seller');
   }
}