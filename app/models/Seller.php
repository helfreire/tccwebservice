 <?php

class Seller extends Eloquent {
    
	protected $hidden = array('password', 'user', 'updated_at');

	public function contact(){
		return $this->hasOne('Contact');
	}

	public function product(){
		return $this->hasOne('Product');
	}

	    /**
     * return relationship with address
     * 
     * @access public
     *
     * @return relationship address
     */
	public function address(){
		return $this->hasOne('Address', 'product_id');
	}

    /**
     * return relationship with categories
     * 
     * @access public
     *
     * @return relationship categories
     */
	public function categories(){
   
        return $this->belongsToMany('Category', 'category_product', 'id');
    }

    /**
     * subcategories
     * 
     * @access public
     *
     * @return relationship subcategories.
     */
    public function subcategories(){
    	return $this->belongsToMany('Subcategory', 'category_product', 'id');
    }

     /**
     * subcategories
     * 
     * @access public
     *
     * @return relationship subcategories.
     */
    public function images(){
        return $this->belongsToMany('Image', 'products', 'id', 'id');
    }

    public function posts(){
        return $this->belongsToMany('Post', 'seller');
    }

}