<?php

class Product extends Eloquent {

    protected $hidden = array('updated_at');

    /**
     * return relationship with address
     * 
     * @access public
     *
     * @return relationship address
     */
	public function address(){
		return $this->hasOne('Address');
	}

    /**
     * return relationship with categories
     * 
     * @access public
     *
     * @return relationship categories
     */
	public function categories(){
   
        return $this->belongsToMany('Category');
    }

    /**
     * subcategories
     * 
     * @access public
     *
     * @return relationship subcategories.
     */
    public function subcategories(){
    	return $this->belongsToMany('Subcategory', 'category_product');
    }

    public function images(){
        return $this->hasOne('Image');
    }

    public function postsDesc(){
        return $this->belongsToMany('Post')->orderBy('created_at', 'DESC');
    }

    public function posts(){
        return $this->belongsToMany('Post');
    }

    public function sellers(){
        return $this->hasOne('Seller', 'id');
    }
}