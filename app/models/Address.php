<?php

class Address extends Eloquent {
    
    public $timestamps = false;
	protected $hidden = array('id', 'created_at', 'updated_at', 'product_id');

   	public function product(){
   		return 	$this->belongsTo('Product');
   	}
}