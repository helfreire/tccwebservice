-- phpMyAdmin SQL Dump
-- version 3.5.8.1deb1
-- http://www.phpmyadmin.net
--
-- Máquina: localhost
-- Data de Criação: 01-Ago-2013 às 21:02
-- Versão do servidor: 5.5.32-0ubuntu0.13.04.1
-- versão do PHP: 5.4.9-4ubuntu2.2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de Dados: `tcc`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `addresses`
--

CREATE TABLE IF NOT EXISTS `addresses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `address` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `addresses_product_id_foreign` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Extraindo dados da tabela `addresses`
--

INSERT INTO `addresses` (`id`, `address`, `phone`, `mobile`, `product_id`) VALUES
(3, 'rua produto 1', '4189899090', '4198988776', 1),
(4, 'rua produto 2', '418989945', '4198988980', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Extraindo dados da tabela `categories`
--

INSERT INTO `categories` (`id`, `category_name`) VALUES
(1, 'Animais'),
(2, 'Bêbes e crianças'),
(3, 'Eletrônicos - Telefonia'),
(4, 'Empregos e negócios'),
(5, 'Esportes'),
(6, 'Hobbies - Música'),
(7, 'Imóveis'),
(8, 'Veículos e barcos'),
(9, 'Moda e Beleza'),
(10, 'Para sua casa');

-- --------------------------------------------------------

--
-- Estrutura da tabela `category_product`
--

CREATE TABLE IF NOT EXISTS `category_product` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `subcategory_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `category_product_product_id_foreign` (`product_id`),
  KEY `category_product_category_id_foreign` (`category_id`),
  KEY `category_product_subcategory_id_foreign` (`subcategory_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `category_product`
--

INSERT INTO `category_product` (`id`, `product_id`, `category_id`, `subcategory_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 2, 2, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contacts`
--

CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `address` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `seller_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `contacts_seller_id_foreign` (`seller_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `contacts`
--

INSERT INTO `contacts` (`id`, `address`, `email`, `phone`, `mobile`, `seller_id`, `created_at`, `updated_at`) VALUES
(1, 'Endereço teste 1', '', '4123344334', '11989888989', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Endereço teste 2', '', '4123344334', '11989888989', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `images`
--

CREATE TABLE IF NOT EXISTS `images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL,
  `image` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `images_product_id_foreign` (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `images`
--

INSERT INTO `images` (`id`, `product_id`, `image`, `quantity`) VALUES
(1, 1, 'http://localhost/images/product-1/', 4),
(2, 2, 'http://localhost/images/product-2/', 4);

-- --------------------------------------------------------

--
-- Estrutura da tabela `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `seller_id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `logs_seller_id_foreign` (`seller_id`),
  KEY `logs_product_id_foreign` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2013_06_14_230208_users', 1),
('2013_06_14_233302_create_sellers_table', 1),
('2013_06_14_234000_create_products_table', 1),
('2013_06_14_234723_contacts', 1),
('2013_06_14_235924_logs', 1),
('2013_06_15_000225_addresses', 1),
('2013_06_15_002216_categories', 1),
('2013_06_15_002627_subcategories', 1),
('2013_06_15_222325_category_product', 1),
('2013_06_18_233112_images', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(5,2) NOT NULL,
  `country` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `seller_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `products_seller_id_foreign` (`seller_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `products`
--

INSERT INTO `products` (`id`, `title`, `description`, `price`, `country`, `city`, `status`, `seller_id`, `created_at`, `updated_at`) VALUES
(1, 'Produto anuncio 1', 'Descrição produto 1', 100.20, 'PR', 'Curitiba', '1', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Produto anuncio 2', 'Descrição produto 2', 200.20, 'SP', 'SAO PAULO', '1', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `sellers`
--

CREATE TABLE IF NOT EXISTS `sellers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `user` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `sellers`
--

INSERT INTO `sellers` (`id`, `firstname`, `lastname`, `user`, `password`, `created_at`, `updated_at`) VALUES
(1, 'Teste', 'um', 'tcc1', '$2y$08$gFtfEmlCqxBOzhwb4Fk7y.s9drT3adVs.QFxIFArm.kxZQKSCWWDm', '2013-07-11 22:44:04', '2013-07-11 22:44:04'),
(2, 'Teste', 'dois', 'tcc2', '$2y$08$arptp3MHr4nKNQoaHlvB0.kMSwjzC/cs5JTo.Fug2KZ3rFRg0H2Y6', '2013-07-11 22:44:04', '2013-07-11 22:44:04');

-- --------------------------------------------------------

--
-- Estrutura da tabela `subcategories`
--

CREATE TABLE IF NOT EXISTS `subcategories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `subcategory_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `subcategories_category_id_foreign` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=43 ;

--
-- Extraindo dados da tabela `subcategories`
--

INSERT INTO `subcategories` (`id`, `category_id`, `subcategory_name`) VALUES
(1, 1, 'Cachorros'),
(2, 1, 'Acessórios'),
(3, 1, 'Outros animais'),
(4, 2, 'Bêbes e crianças'),
(5, 3, 'Celulares e Acessórios'),
(6, 3, 'Computadores e Periféricos'),
(7, 3, 'Video Games - Consoles'),
(8, 3, 'Câmeras e Acessórios'),
(9, 3, 'TV - Áudio -Video e fotografia'),
(10, 4, 'Serviços'),
(11, 4, 'Ofertas de emprego'),
(12, 4, 'Procuro emprego'),
(13, 4, 'Negócio - Indústria - Agro'),
(14, 5, 'Artigos esportivos e ginastica'),
(15, 5, 'Ciclismo'),
(16, 6, 'Livros - Revistas'),
(17, 6, 'Instrumentos Musicais'),
(18, 6, 'Jogos - Brinquedos'),
(19, 6, 'Antiguidades'),
(20, 6, 'Músicas e filmes (DVDs, etc.)'),
(21, 7, 'Vendas - Casas e Apartamentos'),
(22, 7, 'Aluguel - Casas e Apartamentos'),
(23, 7, 'Terrenos - Sítios e Fazendas'),
(24, 7, 'Temporada'),
(25, 7, 'Lojas - Pontos Comerciais'),
(26, 7, 'Imóveis Comerciais - Escritórios e outros'),
(27, 7, 'Aluguel de Quartos'),
(28, 8, 'Carros'),
(29, 8, 'Motos'),
(30, 8, 'Caminhões - Ônibus - Vans'),
(31, 8, 'Peças e Acessórios'),
(32, 8, 'Barcos - Lanhas - Aviões'),
(33, 9, 'Roupas e calçados'),
(34, 9, 'Bijouterias, relógios e joias'),
(35, 9, 'Beleza e Saúde'),
(36, 9, 'Malas, mochilas e acessórios'),
(37, 10, 'Móveis e Decoração'),
(38, 10, 'Eletrodomésticos'),
(39, 10, 'Jardinagem e Construção'),
(40, 10, 'Para Bebês e Crianças'),
(41, 10, 'Cama - Mesa e Banho'),
(42, 10, 'Eletrodomésticos');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `created_at`, `updated_at`) VALUES
(1, 'tcc1', '$2y$08$etCoOVWdxU2EftVxkVaZCuKOiEI4pYyqhFRYWFOSNHJFa5KPa3CX2', '2013-07-11 22:44:04', '2013-07-11 22:44:04'),
(2, 'tcc2', '$2y$08$1YB9nooTnLlNQw5qW5n0KuQzvL4qHAuhbrQhmJenrPm/QGA.7jwZO', '2013-07-11 22:44:04', '2013-07-11 22:44:04');

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `addresses`
--
ALTER TABLE `addresses`
  ADD CONSTRAINT `addresses_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Limitadores para a tabela `category_product`
--
ALTER TABLE `category_product`
  ADD CONSTRAINT `category_product_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `category_product_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `category_product_subcategory_id_foreign` FOREIGN KEY (`subcategory_id`) REFERENCES `subcategories` (`id`);

--
-- Limitadores para a tabela `contacts`
--
ALTER TABLE `contacts`
  ADD CONSTRAINT `contacts_seller_id_foreign` FOREIGN KEY (`seller_id`) REFERENCES `sellers` (`id`);

--
-- Limitadores para a tabela `images`
--
ALTER TABLE `images`
  ADD CONSTRAINT `images_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Limitadores para a tabela `logs`
--
ALTER TABLE `logs`
  ADD CONSTRAINT `logs_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `logs_seller_id_foreign` FOREIGN KEY (`seller_id`) REFERENCES `sellers` (`id`);

--
-- Limitadores para a tabela `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_seller_id_foreign` FOREIGN KEY (`seller_id`) REFERENCES `sellers` (`id`);

--
-- Limitadores para a tabela `subcategories`
--
ALTER TABLE `subcategories`
  ADD CONSTRAINT `subcategories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
